// Appel des modules
const uuidv4 = require('uuid/v4');

// Tableau de données
let posts = 
[
    {
        "id": uuidv4(),
        "title": "La famille Miaou",
        "image": "http://www.maximumwall.com/wp-content/uploads/2017/04/wallpaper-image-chats-en-ete-11-1024x678.jpg",
        "content": "Veniam et nostrud laborum esse commodo. Officia occaecat dolore magna labore minim excepteur. Culpa qui consequat et commodo duis cupidatat labore minim velit. Esse ea culpa exercitation esse nisi magna ipsum et aliqua laborum. Elit ex consectetur qui veniam ad. Exercitation magna proident elit proident excepteur aliqua. Qui voluptate ex aute nisi magna culpa Lorem nisi consequat velit."
    },
    {
        "id": uuidv4(),
        "title": "Mon chat est-il swag ?",
        "image": "http://www.maximumwall.com/wp-content/uploads/2017/04/wallpaper-image-chats-en-ete-05-768x507.jpg",
        "content": "Fugiat enim magna incididunt ad culpa culpa ea esse anim culpa incididunt."
    },
    {
        "id": uuidv4(),
        "title": "Enmener son chat à la plage",
        "image": "http://www.maximumwall.com/wp-content/uploads/2017/04/wallpaper-image-chats-en-ete-13-1024x640.jpg",
        "content": "Sit ex aliqua cupidatat est ea magna esse eu. Mollit esse commodo duis amet adipisicing incididunt dolore nulla labore sunt fugiat. Consequat eiusmod fugiat consectetur Lorem culpa ut ad proident fugiat aliqua elit ad et laborum. Amet ex ex ea nisi sunt aute nostrud laborum veniam laborum voluptate exercitation consequat. Tempor adipisicing id quis labore aute. Ipsum veniam adipisicing dolor amet eu ipsum."
    },
    {
        "id": uuidv4(),
        "title": "Félin perché",
        "image": "http://www.maximumwall.com/wp-content/uploads/2017/04/wallpaper-image-chats-en-ete-17-1024x683.jpg",
        "content": "Esse ad magna minim sint esse est ex enim ullamco anim esse et ipsum. In incididunt aliquip amet ut aute. Pariatur incididunt ex in ullamco. Qui officia nostrud eiusmod magna excepteur."
    }
]

// Récupérer une ligne du tableau
const Post = function (id) {
    return posts.find(r => r.id === id)
}

// Ajouter une ligne au tableau
const AddPost = function (data) {
    posts.push({ id: uuidv4(), ...data })
}

exports.posts = posts
exports.post = Post
exports.addPost = AddPost