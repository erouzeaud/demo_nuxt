// Appel des modules
const express = require('express')
const morgan = require('morgan')
const { posts, post, addPost } = require('./data/posts')
const cors = require('cors')

const host = '0.0.0.0'
const port = 1234

// Initialisation d'Express
const app = express()

app.use(cors())

// Activation du log des routes
app.use(morgan('tiny'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Route d'accueil
app.get('/', (req, res) => {
    res.json({ message: 'Hello World!' })
})

// Routes des posts
app.get('/api/v1/posts', (req, res) => {
    res.json(posts)
})

// Route d'un post
app.get('/api/v1/posts/:id', (req, res) => {
    const id = req.params.id
    const data = post(id)

    if (data) {
        res.json(data)
    } else {
        res.status(404).json({ message: 'not found' })
    }
})

// Ajouter un post
app.post('/api/v1/posts', (req, res) => {
    const { title, content } = req.body;

    if (title && content) {
        addPost(req.body);
        res.status(201).json({ message: 'created' })
    } else {
        res.status(400).json({ mesage: 'missing fields' })
    }
})

// Routes non définies = 404
app.all('/*', (req, res) => {
    res.status(404).json({ message : 'page not found' })
})

// Démarrage du serveur
app.listen(port, host, () => {
    console.log(`Running on http://${host}:${port}`)
})