Projet réalisé avec Express et Nuxt dans le cadre d'une démo le 10 Avril 2019.

## Prérequis

Nodes et NPM installés sur votre machine `node -v && npm -v`.

## Lancement du projet

Dans un terminal, allez dans le dossier de l'application **cd demo_nuxt**, placez-vous dans le dossier  **api** `cd api` puis lancez la commande `npm i` et lancez l'API avec la commande `npm run dev`.  
Le serveur sera accessible à l'adresse http://localhost:1234.

Dans un autre onglet du terminal, allez dans le dossier **front** `cd font` puis lancez la commande `npm i` et lancez l'application front avec la commande `npm run dev`.  
Le serveur sera accessible à l'adresse http://localhost:3000.

Remarque : la commande `npm i` ne s'applique qu'une seule fois.